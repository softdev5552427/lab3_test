/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.manita.lab3_test;

/**
 *
 * @author user
 */
public class Lab3_test {

    static boolean checkWin(String[][] table, String turn, int row, int col) {
        if (checkRow(table, turn, row)) {
            return true;
        }
        if (checkColumn(table, turn, row, col)) {
            return true;
        }
        if(checkX(table,turn,row,col)) {
            return true;
        }
        return false;
    }

    public static boolean checkRow(String[][] table, String turn, int row) {
        for (int j = 0; j < table[row - 1].length; j++) {
            if (!table[row - 1][j].equals(turn)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkColumn(String[][] table, String turn, int row, int col) {

        for (int i = 0; i < table[row - 1].length; i++) {
            if (!table[i][col - 1].equals(turn)) {
                return false;
            }
        }
        return true;

    }

    private static boolean checkX(String[][] table, String turn, int row, int col) {
         if (row - 1 == col - 1) {
            for (int i = 0; i < table.length; i++) {
                if (!table[i][i].equals(turn)) {
                    return false;
                }

            }
            return true;
        }
        if ((row + col) - 2 == table.length - 1) {
            for (int i = 0; i < table.length; i++) {
                if (!table[i][table.length - 1 - i].equals(turn)) {
                    return false;
                }
            }
            return true;
        }
        return false;

    }

    public static boolean checkDraw(String[][] table) {
         for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (table[i][j].equals("-")) {
                    return false;
                }

            }

        }
        return true;
    }

}
