/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.manita.lab3_test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author user
 */
public class OXgameUnittest {

    public OXgameUnittest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_checkRow1_By_X() {
        String[][] table = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String turn = "X";
        int row = 1;
        int col = 1;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));

    }

    @Test
    public void testCheckWin_checkRow2_By_X() {
        String[][] table = {{"-", "-", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        String turn = "X";
        int row = 2;
        int col = 1;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));

    }

    @Test
    public void testCheckWin_checkRow3_By_X() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String turn = "X";
        int row = 3;
        int col = 1;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));

    }

    @Test
    public void testCheckWin_checkCol1_By_O() {
        String[][] table = {{"O", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        String turn = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));
    }

    @Test
    public void testCheckWin_checkCol2_By_O() {
        String[][] table = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String turn = "O";
        int row = 1;
        int col = 2;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));
    }

    @Test
    public void testCheckWin_checkCol3_By_O() {
        String[][] table = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String turn = "O";
        int row = 1;
        int col = 3;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));
    }

    @Test
    public void testCheckWin_checkX1_By_X() {
        String[][] table = {{"X", "-", "-"}, {"-", "X", "-"}, {"-", "-", "X"}};
        String turn = "X";
        int row = 1;
        int col = 1;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));
    }

    @Test
    public void testCheckWin_checkX2_By_X() {
        String[][] table = {{"-", "-", "X"}, {"-", "X", "-"}, {"X", "-", "-"}};
        String turn = "X";
        int row = 1;
        int col = 3;
        assertEquals(true, Lab3_test.checkWin(table, turn, row, col));
    }

    @Test
    public void testCheckWin_checkDraw() {
        String[][] table = {{"X", "O", "X"}, {"X", "X", "O"}, {"O", "X", "O"}};
        assertEquals(true, Lab3_test.checkDraw(table));
    }
}
